# Random Quote Generator

## Description
Selects random quotes from a list and displays them. Allows for adding and removing quotes.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
To run this, download the code and run it in an IDE.

## Roadmap
In the future I want to use SQLite to make a database to store quotes.

