import random
import tkinter as tk

#Quote sets
quotes = [
    "The greatest glory in living lies not in never falling, but in rising every time we fall. - Nelson Mandela",
    "The way to get started is to quit talking and begin doing. - Walt Disney",
    "Your time is limited, don't waste it living someone else's life. - Steve Jobs",
    "Believe you can and you're halfway there. - Theodore Roosevelt",
    "The future belongs to those who believe in the beauty of their dreams. - Eleanor Roosevelt"
]

used_quotes = set()

#Generating Quotes
def generate_quote():
#If "used_quotes" = "quotes", clear "used_quotes" This allows for continuous quote generation but never will put the same quote twice.
    if len(used_quotes) == len(quotes):
        used_quotes.clear()
    remaining_quotes = [quote for quote in quotes if quote not in used_quotes]
    random_quote = random.choice(remaining_quotes)
    used_quotes.add(random_quote)
    quote_label.config(text=random_quote)

#Adding Quotes
def add_quote():
    new_quote = new_quote_entry.get()
    quotes.append(new_quote)
    new_quote_entry.delete(0, tk.END)
    quote_added_label.config(text="Quote added successfully!")

#Deleting Quotes
def delete_quote():
    quote_to_delete = quote_to_delete_entry.get()
    if quote_to_delete in quotes:
        quotes.remove(quote_to_delete)
        quote_deleted_label.config(text="Quote deleted successfully!")
    else:
        quote_deleted_label.config(text="Quote not found in collection.")

root = tk.Tk()
root.title("Quote Generator")

# Generate Quote Section
generate_quote_button = tk.Button(root, text="Generate New", command=generate_quote)
generate_quote_button.pack(pady=10)
quote_label = tk.Label(root, text="")
quote_label.pack()

# Add Quote Section
new_quote_entry = tk.Entry(root)
new_quote_entry.pack(pady=10)
add_quote_button = tk.Button(root, text="Add Quote", command=add_quote)
add_quote_button.pack()
quote_added_label = tk.Label(root, text="")
quote_added_label.pack()

# Delete Quote Section
quote_to_delete_entry = tk.Entry(root)
quote_to_delete_entry.pack(pady=10)
delete_quote_button = tk.Button(root, text="Delete Quote", command=delete_quote)
delete_quote_button.pack()
quote_deleted_label = tk.Label(root, text="")
quote_deleted_label.pack()

root.mainloop()

